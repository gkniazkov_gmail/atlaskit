// @flow

import Item from '@atlaskit/item';
import withItemFocus from '../hoc/withItemFocus';

export default withItemFocus(Item);
