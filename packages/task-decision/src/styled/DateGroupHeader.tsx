import styled from 'styled-components';
import {
  akColorN200,
} from '@atlaskit/util-shared-styles';

// tslint:disable-next-line:variable-name
const DateGroupHeader = styled.div`
  color: ${akColorN200};
  font-size: 12px;
  font-weight: 500;
  margin: 8px;
`;

export default DateGroupHeader;
