# @atlaskit/util-service-support

## Unreleased

## 2.0.1 (2017-07-24)


* fix; make sure types from utils are exports (extracted types to separate file) ([ebde291](https://bitbucket.org/atlassian/atlaskit/commits/ebde291))

## 1.0.0 (2017-07-24)


* feature; extract common service integration code into a shared library ([5714832](https://bitbucket.org/atlassian/atlaskit/commits/5714832))
