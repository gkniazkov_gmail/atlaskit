import styled from 'styled-components';

export const Target = styled.div`
  background-color: orange;
  border-radius: 3px;
  cursor: default;
  display: inline-block;
  height: 30px;
  line-height: 30px;
  padding-left: 1em;
  padding-right: 1em;
  user-select: none;
`;
export const Container = styled.div`
  align-items: center;
  display: flex;
  height: 100vh;
  justify-content: center;
`;
export const Relative = styled.div`
  height: 100px;
  margin-left: 50vw;
  margin-top: 50vh;
  position: relative;
  width: 100px;
`;
