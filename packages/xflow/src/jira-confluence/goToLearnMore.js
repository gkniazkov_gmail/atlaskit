export default () => {
  // redirect user to the confluence pricing calculator
  window.open('https://www.atlassian.com/software/confluence/pricing?tab=cloud', '_blank');
};
